#+TITLE: 圧縮SAT問題について
#+AUTHOR: 田村直之
#+DATE: 2021年6月
#+LANGUAGE: ja
#+OPTIONS: toc:nil
#+LATEX_CLASS: jarticle
#+LATEX_HEADER: \usepackage{orgstyle}
#+LATEX_HEADER: \usepackage{alltt}
#+LATEX_HEADER: \newcommand{\lb}{\ell}
#+LATEX_HEADER: \newcommand{\ub}{u}
#+LATEX_HEADER: \newcommand{\encode}[1]{\left|\,#1\,\right|}
#+LATEX_HEADER: \newcommand{\Equiv}{\Longleftrightarrow}

* 定義

** Finite linear CSP

以下では，有限ドメインの整数変数からなる線形式で構成されるCSP (finite linear CSP)を対象とする．

- 各整数変数 $x$ について，その取り得る値の上下限が定まっているとし，
  下限を $\lb(x)$ ，上限を $\ub(x)$ で表す．
- 各 $x_i$ が整数変数，各 $a_i$ と $c$ が整数定数，
  $\#$ が比較演算子 ($<$, $\le$, $>$, $\ge$, $=$)のとき
  $\sum_{i=1}^{n} a_i x_i \# c$ を線形比較と呼ぶ．
- 命題変数，命題変数の否定，および線形比較をCSPリテラル (あるいは単にリテラル)と呼ぶ．
- 複数のCSPリテラルの論理和をCSP節 (あるいは単に節)と呼ぶ．
- 複数のCSP節の論理積をCSP-CNF式 (あるいは単にCNF式)と呼ぶ．

整数変数からなる数式 $E$ の下限と上限をそれぞれ $\lb(E)$ と $\ub(E)$ で表す．
また，値割当て $\alpha$ における数式 $E$ の下限と上限を $\lb_{\alpha}(E)$ と $\ub_{\alpha}(E)$ で表す．

** CSP-CNF式の正規形

CNF式中に $n$ 変数の線形比較 $\sum_{i=1}^{n} a_i x_i \# c$ \ $(n>3)$ が現れている場合，
新しい補助整数変数 $y$ と線形比較 $y=a_1 x_1 + a_2 x_2$ をCNF式に追加することで，
元の線形比較を $n-1$ 変数の線形比較 $y+\sum_{i=3}^{n} a_i x_i \# c$ に置き換えることができる．
したがって，これを繰り返せばすべての線形比較を3変数以下にすることが可能である．

さらに，式変形によって線形比較を $\sum_{i=1}^{n} a_i x_i \le c$ の形にできる．

CNF節中の線形比較 $\sum_{i=1}^{n} a_i x_i \le c$ は，
この線形比較を新しい命題変数 $p$ に置き換えて
$\neg p \lor \sum_{i=1}^{n} a_i x_i \le c$ をCNF式に付け加えても充足同値である
(Tseitin変換)．

したがって，CNF節を以下のいずれかの形式に制限してもよい．

1. $\displaystyle \sum_{i=1}^{n} a_i x_i \le c$ \quad $(n \le 3)$
2. $\displaystyle \lnot p \lor \sum_{i=1}^{n} a_i x_i \le c$ \quad $(n \le 3)$
3. $\displaystyle \bigvee_{i=1}^{n} p_i$

*** メモ

- $\lnot p \lor \sum_{i=1}^{n} a_i x_i \le c$ は，
  $(\sum_{i=1}^{n} a_i x_i) - c$ の最大値を $c'$ とすると
  $(\sum_{i=1}^{n} a_i x_i) - c \le c' (1-p)$ と同値になる
  (命題変数 $p$ を0-1変数とみなす)．
  また，この線形制約が4変数以上なら，補助整数変数を導入することで3変数にできる．
  したがって，上記のCNF節の制限は1, 3の形式だけに限ることも可能である．
- また3の $\bigvee_{i=1}^{n} p_i$ は $\sum_{i=1}^{n} p_i \ge 1$ と同値だから
  すべてを1の形式することも可能である．
- 新しい変数 $x'$ を導入して $x \mapsto x'+\lb(x)$ の置換を行えば，
  $\lb(x') = 0$, $\ub(x') = \ub(x)-\lb(x)$ だからすべての変数の下限を0にすることができる．

*** 検討事項

- どの部分式を新しい変数 $y$ で置き換えるのが良いか
- メモの方法を用いるのが良いかどうか

* 線形比較の順序符号化

線形比較 $\sum_i a_i x_i \le c$ を順序符号化した結果の命題論理CNF式を
$\encode{\sum_i a_i x_i \le c}$ で表すことにする．

$\lb(x) \le c < \ub(x)$ のとき，
線形比較 $x \le c$ を順序符号化した結果は以下のようになる．

\begin{align*}
\encode{x \le c} & \Equiv \left\{
\begin{array}{ll}
  \bot & (c < \lb(x)) \\
  p(x\le c) & (\lb(x) \le c < \ub(x)) \\
  \top & (\ub(x) \le c)
\end{array}
\right.
\end{align*}

ただし $p(x \le c)$ は，
順序符号化で $x \le c$ を表すために導入した命題変数である．

また $\encode{x < c}$, $\encode{x > c}$, $\encode{x \ge c}$ を以下のように定義する．

\begin{align*}
\encode{x < c} & \Equiv \encode{x \le c-1} \\
\encode{x > c} & \Equiv \lnot \encode{x \le c} \\
\encode{x \ge c} & \Equiv \lnot \encode{x \le c-1}
\end{align*}

簡単のため，以下では線形比較 $\sum_{i=1}^{n} a_i x_i \le c$ \ $(n \le 3)$ に関し
$a_i>0$ の場合についてだけ説明する．

このとき $p(x\le c)$ の形の正リテラルだけが現れることに注意する．

** 1変数線形比較の場合

\begin{align*}
\encode{a_1 x_1 \le c}
& \Equiv \encode{x_1 \le \lfloor c/a_1 \rfloor}
\end{align*}

** 2変数線形比較の場合

\begin{align*}
\encode{a_1 x_1 + a_2 x_2\le c}
& \Equiv
  \bigwedge_{b_1=\lb(x_1)}^{\ub(x_1)}
  x_1 \ge b_1 \Rightarrow a_2 x_2 \le c-a_1 b_1 \\
& \Equiv
  \bigwedge_{b_1=\lb(x_1)}^{\ub(x_1)}
  \encode{x_1 < b_1} \lor \encode{x_2 \le \lfloor (c-a_1 b_1)/a_2 \rfloor}
\end{align*}

2番目のリテラル $\encode{x_2 \le \lfloor (c-a_1 b_1)/a_2 \rfloor}$ は，
$c-a_1 b_1 < a_2 \lb(x_2)$ のときに恒偽で，
$c-a_1 b_1 \ge a_2 \ub(x_2)$ のときに恒真である．

  - 恒偽になる場合，最初のリテラル $\encode{x_1 < b_1}$ だけが残り，
    そのうちで最小の $b_1$ だけが必要である．
    $a_1 b_1 \ge c - a_2 \lb(x_2) - 1$ を満たす最小の $b_1$ は
    $\lceil (c - a_2 \lb(x_2) - 1)/a_1 \rceil$ である．
  - 恒真になる場合の節はすべて削除できる．
    すなわち $b_1 \le \lfloor (c - a_2 \ub(x_2))/a_1 \rfloor$ を満たす場合である．

したがって
\begin{align*}
  \lb_1 & = \max(\lb(x_1),\ \lfloor (c - a_2 \ub(x_2))/a_1 \rfloor+1) \\
  \ub_1 & = \min(\ub(x_1),\ \lceil (c - a_2 \lb(x_2) - 1)/a_1 \rceil)
\end{align*}
として，
\begin{align*}
\encode{a_1 x_1 + a_2 x_2\le c}
& \Equiv
  \bigwedge_{b_1=\lb_1}^{\ub_1}
  \encode{x_1 < b_1} \lor \encode{x_2 \le \lfloor (c-a_1 b_1)/a_2 \rfloor}
\end{align*}
である．

*** 検討事項

- どの項を $a_1 x_1$ として選ぶべきか

*** COMMENT 

このとき $c$ と $a_1 b_1$ のそれぞれを $a_2$ で割った商と余りを考え，以下のように表す．

\begin{align*}
c & = a_2 q+r \quad (0\le r <a_2) \\
a_1 b_1 & =a_2 q_1+r_1=a_2(q_1+1)-a_2+r_1 \quad (0\le r_1<a_2)
\end{align*}

すると $(c-a_1 b_1)/a_2 = q-q_1-1+(r+a_2-r_1)/a_2$ であり，
$0 \le r+a_2-r_1 < 2a_2$ だから，以下が成り立つ．

\begin{align*}
\lfloor (c-a_1 b_1)/a_2 \rfloor & = q-q_1-[r<r_1]
\end{align*}

ただし $[\phi]$ は $\phi$ が真のとき1，偽のとき0となる関数である．

** 3変数線形比較の場合

\begin{align*}
\encode{a_1 x_1 + a_2 x_2 + a_3 x_3 \le c}
& \Equiv
  \bigwedge_{b_1=\lb(x_1)}^{\ub(x_1)}\bigwedge_{b_2=\lb(x_2)}^{\ub(x_2)}
  x_1 \ge b_1 \Rightarrow x_2 \ge b_2 \Rightarrow a_3 x_3 \le c-a_1 b_1-a_2 b_2 \\
& \Equiv
  \bigwedge_{b_1=\lb(x_1)}^{\ub(x_1)}\bigwedge_{b_2=\lb(x_2)}^{\ub(x_2)}
  \encode{x_1 < b_1} \lor \encode{x_2 < b_2} \lor \encode{x_3 \le \lfloor (c-a_1 b_1-a_2 b_2)/a_3 \rfloor}
\end{align*}

2変数の場合と同様に，3変数での $b_1$, $b_2$ の上下限も評価できる．

*** 検討事項

- どの項を $a_1 x_1$, $a_2 x_2$ として選ぶべきか

*** COMMENT 

2変数の場合と同様に $c$, $a_1 b_1$, $a_2 b_2$ のそれぞれを $a_3$ で割った商と余りを考え，以下のように表す．

\begin{align*}
c & = a_3 q+r \quad (0\le r <a_3) \\
a_1 b_1 & =a_3 q_1+r_1=a_3(q_1+1)-a_3+r_1 \quad (0\le r_1<a_3) \\
a_2 b_2 & =a_3 q_2+r_2=a_3(q_2+1)-a_3+r_2 \quad (0\le r_2<a_3)
\end{align*}

すると $(c-a_1 b_1-a_2 b_2)/a_3 = q-q_1-1-q_2-1+(r+a_3-r_1+a_3-r_2)/a_3$ であり，
$0 \le r+a_3-r_1+a_3-r_2 < 3a_3$ だから，以下が成り立つ．

\begin{align*}
\lfloor (c-a_1 b_1-a_2 b_2)/a_3 \rfloor =
  q-q_1-q_2-[r<r_1+r_2]-[r+a_3<r_1+r_2]
\end{align*}


\end{document}

* 線形比較に対する伝播

ここでは，線形比較の順序符号化を経ずに，直接的に
1の形式のCNF節 $\sum_{i=1}^{n} a_i x_i \le c$ に対して
衝突検知と伝播を行う方法を検討する．

線形比較 $\sum_i a_i x_i \le c$ において，
$a_i>0$ なら $x_i$ の下限が更新されたときにだけ，
$a_i<0$ なら $x_i$ の上限が更新されたときにだけ
衝突あるいは伝播が生じることに注意する．

関数 \texttt{getLB}($a$, $x$) は，現在の値割当て $\alpha$ の元での式 $ax$ の下限値 $\lb_{\alpha}(ax)$ を返す．

\begin{alltt}
function getLB(\(a\), \(x\)) \{
  return \(a\) < 0 ? \(a\cdot\ub\sb{\alpha}(x)\) : \(a\cdot\lb\sb{\alpha}(x)\)
\}
\end{alltt}

関数 \texttt{getUB}($a$, $x$) は，現在の値割当て $\alpha$ の元での式 $ax$ の上限値 $\ub_{\alpha}(ax)$ を返す．

\begin{alltt}
function getUB(\(a\), \(x\)) \{
  return \(a\) < 0 ? \(a\cdot\lb\sb{\alpha}(x)\) : \(a\cdot\ub\sb{\alpha}(x)\)
\}
\end{alltt}

関数 \texttt{getPropagate}($a$, $x$, $c$) は，現在の値割当て $\alpha$ の元で
$ax \le c$ を満たすために伝播すべきリテラルを返す．

\begin{alltt}
function getPropagate(\(a\), \(x\), \(c\)) \{
  if (getLB(\(a\), \(x\)) > \(c\)) return \(\bot\)
  else if (getUB(\(a\), \(x\)) <= \(c\)) return \(\top\)
  else return \(\encode{ax\le{}c}\)
\}
\end{alltt}

** 2変数線形比較の場合

以下のように衝突検知と伝播を実装できる．

1. $a_i$ が正なら $x_i$ の下限の更新を，負なら上限の更新を監視する．
2. 上下限が更新されれば以下を行う．
   - $b_1 = \texttt{getLB}(a_1, x_1)$ と置く
   - $b_2 = \texttt{getLB}(a_2, x_2)$ と置く
3. $x_1$ に対する伝播処理
   - $l_1 = \texttt{getPropagate}(a_1, x_1, c-b_2)$ と置く
   - $l_1 = \bot$ なら衝突
   - $l_1 = \top$ なら何もしない
   - それ以外なら $l_1 = \encode{a_1 x_1 \le c-b_2}$ を伝播．
     reasonは $l_1\lor\encode{a_2 x_2 < b_2}$ (正しい?)
4. $x_2$ に対する伝播処理も同様

*** 検討事項

- 正しい?
- 2の形式のCNF節 $\lnot p \lor \sum_{i=1}^{n} a_i x_i \le c$ に対してはどうなるか?

** 3変数線形比較の場合

以下のように衝突検知と伝播を実装できる．

1. $a_i$ が正なら $x_i$ の下限の更新を，負なら上限の更新を監視する．
2. 上下限が更新されれば以下を行う．
   - $b_1 = \texttt{getLB}(a_1, x_1)$ と置く
   - $b_2 = \texttt{getLB}(a_2, x_2)$ と置く
   - $b_3 = \texttt{getLB}(a_3, x_3)$ と置く
3. $x_1$ に対する伝播処理
   - $l_1 = \texttt{getPropagate}(a_1, x_1, c-b_2-b_3)$ と置く
   - $l_1 = \bot$ なら衝突
   - $l_1 = \top$ なら何もしない
   - それ以外なら $l_1 = \encode{a_1 x_1 \le c-b_2-b_3}$ を伝播．
     reasonは $l_1\lor\encode{a_2 x_2 < b_2}\lor\encode{a_3 x_3 < b_3}$ (正しい?)
4. $x_2$, $x_3$ に対する伝播処理も同様

*** 検討事項

- 正しい?
- 2の形式のCNF節 $\lnot p \lor \sum_{i=1}^{n} a_i x_i \le c$ に対してはどうなるか?
- $l_1 = \encode{a_1 x_1 \le c-b_2-b_3}$ が伝播される原因は
  $a_2 x_2 + a_3 x_3 \ge b_2 + b_3$ である．
  したがって，BDD風に $a_2 x_2 + a_3 x_3 < b_2 + b_3$ を意味する新しい命題変数を導入する方法が考えられる．

